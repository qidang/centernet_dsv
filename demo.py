import os
import argparse
import platform
import numpy as np
import datetime
from torchcore.tools import Logger
import torchvision
import math
from torch import nn
import json
import tqdm
import time
from PIL import Image, ImageDraw

from pycocotools.coco import COCO
from pycocotools.cocoeval import COCOeval

from PIL.ImageDraw import Draw

from rcnn_config import config
from torchcore.tools import torch_tools

import torch
import torchvision.transforms as transforms
import torch.optim as optim

#from torchcore.dnn import trainer
from torchcore.dnn import networks
#from torchcore.data.transforms import Compose, RandomCrop, RandomScale, RandomMirror, ToTensor, Normalize
from torchcore.data.datasets import COCOPersonCenterDataset, DemoDataset
from torchcore.dnn.networks.center_net import CenterNet
#from torchcore.dnn.networks.tools.data_parallel import DataParallel

def parse_commandline():
    parser = argparse.ArgumentParser(description="Training the Model")
    #parser.add_argument('-c','--config',help='Configuration Name', required=True)
    #parser.add_argument('--patchsize',help='Patch Size', required=True)
    #parser.add_argument('-b','--batch_size',help='Batch size', required=True)
    #parser.add_argument('-t','--tag',help='Model tag', required=False)
    parser.add_argument('--resume',help='resume the model', default=False, required=False)
    #parser.add_argument('-g','--gpu',help='GPU Index', default='0')
    #parser.add_argument('--projectpath',help='Path to the project',required=True)
    return parser.parse_args()

def load_checkpoint(model, path, device, to_print=True):
    #checkpoint = torch.load(path)
    state_dict_ = torch.load(path, map_location=device)['model_state_dict']
    state_dict = {}
    for k in state_dict_:
        if k.startswith('module') and not k.startswith('module_list'):
            state_dict[k[7:]] = state_dict_[k]
        else:
            state_dict[k] = state_dict_[k]
    model.load_state_dict(state_dict, strict=True )
    if to_print:
        print('Chekpoint has been loaded from {}'.format(path))


def set_device( blobs, device):
    if type(blobs) == list:
        for i in range(len(blobs)):
            blobs[i] = set_device(blobs[i], device)
    elif type(blobs) == dict:
        for key, data in blobs.items():
            blobs[key] = set_device(data, device)
    elif torch.is_tensor(blobs):
        blobs = blobs.to(device)
    return blobs

def demo(dataset, model, device, threshold=0.3):
    model.eval()
    with torch.no_grad() :
        for idx, inputs in enumerate(tqdm.tqdm(dataset, 'evaluating')):
            inputs = set_device( inputs, device )
            output = model( inputs)
            batch_size = len(output['boxes'])
            batch_boxes = output['boxes']
            batch_scores = output['scores']
            batch_labels = output['category']
            ori_im = Image.fromarray(inputs['ori_image'][idx].detach().cpu().numpy())
            draw = Draw(ori_im)
            for boxes, scores in zip(batch_boxes, batch_scores):
                scores = scores.detach().cpu().numpy()
                ind = scores > threshold
                boxes = boxes.detach().cpu().numpy()
                boxes = boxes[ind]
                for box in boxes:
                    draw.rectangle(box, outline=(255, 0, 0))
            ori_im.show()


if __name__=="__main__" :
    #args = parse_commandline()
    weight_path = 'weights/checkpoints_20200305_centernet_r3_150.pkl'
    threshold = 0.35

    # prepare dataset
    demo_image_paths = ['demo_imgs/1.jpg']
    dataset = DemoDataset(demo_image_paths)
    data_loader = torch.utils.data.DataLoader(
      dataset, 
      batch_size=1, 
      shuffle=False,
      num_workers=0,
      pin_memory=True,
      drop_last=False
    )

    # build model
    backbone = networks.feature.resnet50()
    in_channel = backbone.out_channel
    neck = networks.neck['upsample_basic'](in_channel)
    backbone.multi_feature = False
    parts = ['heatmap', 'offset', 'width_height']
    model = CenterNet(backbone, 1, neck=neck, parts=parts)

    # get device
    use_cuda = torch.cuda.is_available()
    device_name = "cuda" if use_cuda else "cpu"
    device = torch.device( device_name )

    load_checkpoint(model, weight_path, device)

    demo(data_loader, model, device, threshold=threshold)

