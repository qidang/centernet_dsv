import os
import argparse
import platform
import numpy as np
import datetime
from torchcore.tools import Logger
import torchvision
import math
from torch import nn
import json
import tqdm
import time

from pycocotools.coco import COCO
from pycocotools.cocoeval import COCOeval

from PIL.ImageDraw import Draw

from rcnn_config import config
from torchcore.tools import torch_tools

import torch
import torchvision.transforms as transforms
import torch.optim as optim

from torchcore.dnn import trainer
from torchcore.dnn import networks
from torchcore.data.transforms import Compose, RandomCrop, RandomScale, RandomMirror, ToTensor, Normalize
from torchcore.data.datasets import COCOPersonCenterDataset, DemoDataset, COCOCenterDataset, ModanetCenterDataset, MixDataset
from torchcore.dnn.networks.center_net import CenterNet
from torchcore.dnn.networks.center_net_mul_branch import CenterNetMulBranch
from torchcore.dnn.networks.tools.data_parallel import DataParallel
from torchcore.data.sampler import MixBatchSampler
from torchcore.data.collate import mix_dataset_collate

from collections import OrderedDict

def parse_commandline():
    parser = argparse.ArgumentParser(description="Training the Model")
    #parser.add_argument('-c','--config',help='Configuration Name', required=True)
    #parser.add_argument('--patchsize',help='Patch Size', required=True)
    parser.add_argument('-b','--batch_size',help='Batch size', default=16, required=True, type=int)
    parser.add_argument('-d','--dataset',help='dataset name', default='coco', required=False, type=str)
    #parser.add_argument('-t','--tag',help='Model tag', required=False)
    parser.add_argument('--resume',help='resume the model', action='store_true')
    parser.add_argument('--gpus',help='resume the model', default='-1', required=False)
    #parser.add_argument('-g','--gpu',help='GPU Index', default='0')
    #parser.add_argument('--datasetpath',help='Path to the dataset',required=True)
    #parser.add_argument('--projectpath',help='Path to the project',required=True)
    return parser.parse_args()

def get_absolute_box(human_box, box):
    #box[2]+=int(human_box[0])+box[0]
    #box[3]+=int(human_box[1])+box[1]
    box[0]+=int(human_box[0])
    box[1]+=int(human_box[1])
    return box

class my_trainer(trainer):
    def __init__( self, cfg, model, device, trainset, testset=None, dataset_name='modanet', benchmark=None, criterias=None, val_dataset=None, gpus=None ):
        self._cfg = cfg
        self._device = torch.device(device)
        self._optimizer = None
        self._model = model
        if hasattr(model, 'num_branches'):
            self.num_branches = model.num_branches

        self._trainset = trainset
        self._testset = testset
        self._benchmark = benchmark
        self._criterias = criterias
        self._dataset_name = dataset_name
        self._epoch = 0
        self._val_dataset = val_dataset
        #self._trainset_feeder = data_feeder( trainset )

        #if testset is not None :
        #    self._testset_feeder = data_feeder( testset )

        self._set_optimizer()
        if cfg.resume:
            path = os.path.join(os.path.dirname(cfg.path.CHECKPOINT), 'last.pth') 
            self.resume_training(path, device)

        if 'cuda' in device:
            model = DataParallel(model,
                device_ids=gpus, 
                chunk_sizes=None)
        model.to(self._device)
        self._model = model

        self.init_logger()

    def train( self ):
        #if self._testset is not None :
        #    self._validate()

        for i in range( self._epoch+1, self._niter+1 ):
            print("Epoch %d/%d" % (i,self._niter))
            self._logger.info('epoch {}\n'.format(i))
            self._train()

            if self._testset is not None and i%15 == 0 :
                self._validate()
            self._epoch = i
            self.save_training(self._cfg.path.CHECKPOINT.format(self._epoch))
            self._scheduler.step()

    def train_mul_branch( self, test_branches=None ):
        #if self._testset is not None :
        #    self._validate_multi_branch(test_branches)

        for i in range( self._epoch+1, self._niter+1 ):
            print("Epoch %d/%d" % (i,self._niter))
            self._logger.info('epoch {}\n'.format(i))
            self._train()

            if self._testset is not None and i%15 == 0 :
                self._validate_multi_branch(test_branches)
            self._epoch = i
            self.save_training(self._cfg.path.CHECKPOINT.format(self._epoch))
            self._scheduler.step()


    def train_one_epoch(self):
        self._train()

    def validate_onece(self):
        self._validate()

    def _validate_multi_branch(self, test_branches=None):
        if test_branches is None:
            test_branches = list(range(self.num_branches))

        for i in test_branches:
            self._validate_single_branch(i)

    def _validate_single_branch( self, branch ):
        print('start to validate branch {}: {}'.format(branch, self._dataset_name[branch]))
        self._model.eval()
        if isinstance(self._model, torch.nn.DataParallel):
            self._model.module.set_output_branch([branch])
        else:
            self._model.set_output_branch([branch])

        results = []
        with torch.no_grad():
            for idx,(inputs, targets) in enumerate(tqdm.tqdm(self._testset[branch], 'evaluating')):
                inputs['dataset_label'] = torch.ones(len(inputs['data']))*branch
                inputs = self._set_device( inputs )
                output = self._model( inputs)[0]
                batch_size = len(output['boxes'])
                #for i, im in enumerate(output):
                for i in range(batch_size):
                    if len(output['boxes'][i]) == 0:
                        continue
                    # convert to xywh
                    output['boxes'][i][:,2] -= output['boxes'][i][:,0]
                    output['boxes'][i][:,3] -= output['boxes'][i][:,1]
                    for j in range(len(output['boxes'][i])):
                        results.append({'image_id':int(targets['image_id'][i]), 
                                        'category_id':output['category'][i][j].cpu().numpy().tolist()+1, 
                                        'bbox':output['boxes'][i][j].cpu().numpy().tolist(), 
                                        'score':output['scores'][i][j].cpu().numpy().tolist()})
                #output = self._model['net']( inputs, just_embedding=True) # debug
                #bench.update( targets, output )
        with open('temp_result.json','w') as f:
            json.dump(results,f)
        self.eval_result(dataset=self._dataset_name[branch])

    def _validate( self ):
        print('start to validate')
        self._model.eval()

        results = []
        with torch.no_grad() :
            for idx,(inputs, targets) in enumerate(tqdm.tqdm(self._testset, 'evaluating')):
                inputs = self._set_device( inputs )
                output = self._model( inputs)
                batch_size = len(output['boxes'])
                #for i, im in enumerate(output):
                for i in range(batch_size):
                    if len(output['boxes'][i]) == 0:
                        continue
                    # convert to xywh
                    output['boxes'][i][:,2] -= output['boxes'][i][:,0]
                    output['boxes'][i][:,3] -= output['boxes'][i][:,1]
                    for j in range(len(output['boxes'][i])):
                        results.append({'image_id':int(targets['image_id'][i]), 
                                        'category_id':output['category'][i][j].cpu().numpy().tolist()+1, 
                                        'bbox':output['boxes'][i][j].cpu().numpy().tolist(), 
                                        'score':output['scores'][i][j].cpu().numpy().tolist()})
                #output = self._model['net']( inputs, just_embedding=True) # debug
                #bench.update( targets, output )
        with open('temp_result.json','w') as f:
            json.dump(results,f)
        self.eval_result(dataset=self._dataset_name)
    

    def eval_result(self, dataset='coco_person'):
        if dataset not in ['coco','coco_person', 'modanet']:
            raise ValueError('only support coco_person and modanet dataset')
        if dataset == 'coco_person':
            gt_json=os.path.expanduser('~/data/datasets/COCO/annotations/instances_val2014.json')
        if dataset == 'coco':
            gt_json=os.path.expanduser('~/data/datasets/COCO/annotations/instances_val2017.json')
        else:
            gt_json=os.path.expanduser('~/data/datasets/modanet/Annots/modanet_instances_val.json')
        dt_json='temp_result.json'

        # we need to map the category ids back
        if dataset == 'coco':
            cat_ids = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 27, 28, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 67, 70, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 84, 85, 86, 87, 88, 89, 90]
            with open(dt_json) as f:
                results = json.load(f)
                for result in results:
                    temp_id = result['category_id']
                    result['category_id'] = cat_ids[temp_id-1]
            with open('temp_result.json','w') as f:
                json.dump(results,f)

        annType = 'bbox'
        cocoGt=COCO(gt_json)
        cocoDt=cocoGt.loadRes(dt_json)

        imgIds=sorted(cocoGt.getImgIds())

        # running evaluation
        cocoEval = COCOeval(cocoGt,cocoDt,annType)
        if dataset == 'coco_person':
            cocoEval.params.catIds = [1]
        cocoEval.params.imgIds = imgIds
        cocoEval.evaluate()
        cocoEval.accumulate()
        cocoEval.summarize()

    def _train( self ):
        self._model.train()

        loss_values = []
        average_losses = {}

        for idx, (inputs, targets) in enumerate(tqdm.tqdm(self._trainset, desc='Training')):
            #print('inputs:', inputs.keys())
            #print('targets:', targets.keys())

            inputs = self._set_device( inputs )
            targets = self._set_device( targets )
            #print('input device:', inputs[0].device)

            loss_dict = self._model(inputs, targets)

            # add the losses for each part
            #loss_sum = sum(loss for loss in loss_dict.values())
            loss_sum=0
            for single_loss in loss_dict:
                loss_sum = loss_sum + loss_dict[single_loss]
                if single_loss in average_losses:
                    average_losses[single_loss]+= loss_dict[single_loss].mean()
                else:
                    average_losses[single_loss] = loss_dict[single_loss].mean()

            loss_sum = loss_sum.mean()
            if not math.isfinite(loss_sum):
                #print("Loss is {}, stopping training".format(loss_sum))
                print("Loss is {}, skip this batch".format(loss_sum))
                print(loss_dict)
                continue
                #sys.exit(1)

            # Computing gradient and do SGD step
            self._optimizer.zero_grad()
            loss_sum.backward()
            self._optimizer.step()
            loss_values.append( loss_sum.cpu().detach().numpy() )

            if idx%100 == 0:
                self._logger.info('{} '.format(idx+1))
                loss_str = ''
                for loss in average_losses:
                    if idx==0:
                        loss_num = average_losses[loss]
                    else:
                        loss_num = average_losses[loss] / 100
                    self._logger.info('{} '.format(loss_num))
                    loss_str += (' {} loss:{}, '.format(loss, loss_num))
                print(loss_str[:-2])
                average_losses = {}
                self._logger.info('\n')

            #bar.update(idx+1,loss=loss_sum.item())
        #bar.finish()

        print('Average loss : ', np.mean(loss_values))

    def _set_device( self, blobs ):
        if type(blobs) == list:
            for i in range(len(blobs)):
                blobs[i] = self._set_device(blobs[i])
        elif type(blobs) == dict:
            for key, data in blobs.items():
                blobs[key] = self._set_device(data)
        elif torch.is_tensor(blobs):
            blobs = blobs.to(self._device)
        return blobs

    def init_logger(self):
        train_path = self._cfg.path['LOG']

        console_formatter = '{} {{}}'.format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        self._logger = Logger(level='info', file=train_path, console=False, console_formatter=console_formatter)

        print('Loss log path is: {}'.format(train_path))

    def save_training(self, path, to_print=True):
        if isinstance(model, torch.nn.DataParallel):
            state_dict = self._model.module.state_dict()
        else:
            state_dict =self._model.state_dict()
        torch.save({
            'epoch': self._epoch,
            'model_state_dict': state_dict,
            'optimizer_state_dict': self._optimizer.state_dict(),
            'scheduler':self._scheduler.state_dict(),
            'niter':self._niter
        }, path)
        folder = os.path.dirname(path)
        torch.save({
            'epoch': self._epoch,
            'model_state_dict': state_dict,
            'optimizer_state_dict': self._optimizer.state_dict(),
            'scheduler':self._scheduler.state_dict(),
            'niter':self._niter
        }, os.path.join(folder, 'last.pth'))
        if to_print:
            print('The checkpoint has been saved to {}'.format(path))

    def resume_training(self, path, device, to_print=True):
        checkpoint = torch.load(path, map_location=device)
        self._epoch = checkpoint['epoch']
        #print(checkpoint['model_state_dict'].keys())
        state_dict = checkpoint['model_state_dict']
        state_dict_new = OrderedDict()
        for k, v in state_dict.items():
            if k[:6]=='module':
                state_dict_new[k[7:]] = state_dict[k]
            else:
                state_dict_new[k] = state_dict[k]

        self._model.load_state_dict(state_dict_new)
        self._optimizer.load_state_dict(checkpoint['optimizer_state_dict'] )
        for state in self._optimizer.state.values():
            for k, v in state.items():
                if torch.is_tensor(v):
                    #state[k] = v.cuda()
                    state[k] = v.to(device)
        self._niter = self._niter
        #if 'scheduler' in checkpoint:
        self._scheduler.load_state_dict(checkpoint['scheduler'])
        if to_print:
            print('Chekpoint has been loaded from {}'.format(path))

if __name__=="__main__" :
    args = parse_commandline()
    params = {}
    params['config'] = 'center_net_coco'
    #params['nclasses'] = 13
    #params['gpu'] = '0'
    params['tag'] = '20200905'
    params['path'] = {}

    params['path']['project'] = os.path.expanduser('~/Vision/data')
    dataset_name = args.dataset
    #path to load data
    if platform.system() == 'Linux':
        params['path']['dataset'] = '/ssd/data'
    else:
        params['path']['dataset'] = '/Users/qida0163/Vision/data'

    #device = torch_tools.get_device( params['gpu'] )

    cfg = config( params['config'], 'train', params['path']['dataset'], params['path']['project'] )
    cfg.update( params )
    cfg.resume = args.resume
    gpus = [int(gpu) for gpu in args.gpus.split(',')]
    gpus = gpus if gpus[0] >=0 else None
    

    #set the paths to save all the results (model, val result)
    #cfg.build_path( params['tag'], 'coco_person', model_hash='centernet' )
    cfg.build_path( params['tag'], dataset_name, model_hash='centernet' )

    #anno_path = os.path.expanduser('./data/annotations/coco2014_instances_person.pkl')

    train_transform_list = []
    random_crop = RandomCrop((512,512)) #(width, height)
    #random_crop = RandomCrop(512)
    random_scale = RandomScale(0.6, 1.4)
    random_mirror = RandomMirror()
    to_tensor= ToTensor()
    normalize = Normalize()
    train_transform_list.append(random_scale)
    train_transform_list.append(random_crop)
    train_transform_list.append(random_mirror)
    #transform_list.append(to_tensor)
    #transform_list.append(normalize)
    # the to tensor and normalize transformation are done default on the COCOPersonCenterDataset
    train_transforms = Compose(train_transform_list)

    if dataset_name == 'coco':
        anno_path = os.path.expanduser('./data/annotations/coco2017_instances.pkl')
        root = os.path.expanduser('./data/datasets/COCO')
        dataset = COCOCenterDataset(root=root, anno=anno_path, part='train2017',transforms=train_transforms)
        test_dataset = COCOCenterDataset(root=root, anno=anno_path, part='val2017',transforms=None, training=False)
        class_num = 80
    elif dataset_name == 'modanet':
        anno_path = os.path.expanduser('./data/annotations/modanet2018_instances_revised.pkl')
        root = os.path.expanduser('./data/datasets/modanet/Images/')
        dataset = ModanetCenterDataset(root=root, anno=anno_path, part='train',transforms=train_transforms)
        test_dataset = ModanetCenterDataset(root=root, anno=anno_path, part='val',transforms=None, training=False)
        class_num = 13
    elif dataset_name == 'mix':
        anno_path = os.path.expanduser('./data/annotations/coco2017_instances.pkl')
        root = os.path.expanduser('./data/datasets/COCO')
        dataset_a = COCOCenterDataset(root=root, anno=anno_path, part='train2017',transforms=train_transforms)
        test_dataset_a = COCOCenterDataset(root=root, anno=anno_path, part='val2017',transforms=None, training=False)
        anno_path = os.path.expanduser('./data/annotations/modanet2018_instances_revised.pkl')
        root = os.path.expanduser('./data/datasets/modanet/Images/')
        dataset_b = ModanetCenterDataset(root=root, anno=anno_path, part='train',transforms=train_transforms)
        test_dataset_b = ModanetCenterDataset(root=root, anno=anno_path, part='val',transforms=None, training=False)
        dataset = MixDataset(dataset_a, dataset_b, add_dataset_label=True)
        test_dataset = MixDataset(test_dataset_a, test_dataset_b, add_dataset_label=True)
        class_nums = [80, 13]

    #dataset = COCOPersonCenterDataset(root=root, anno=anno_path, part='train2014',transforms=train_transforms)
    #test_dataset = COCOPersonCenterDataset(root=root, anno=anno_path, part='val2014',transforms=None, training=False)

    if dataset_name != 'mix':
        data_loader = torch.utils.data.DataLoader(
        dataset, 
        batch_size=args.batch_size, 
        shuffle=True,
        num_workers=30,
        pin_memory=True,
        drop_last=True
        )

        val_data_loader = torch.utils.data.DataLoader(
        test_dataset, 
        batch_size=1, 
        shuffle=False,
        num_workers=4,
        pin_memory=True,
        drop_last=False
        )
    else:
        sampler = MixBatchSampler([len(dataset_a), len(dataset_b)], mode='balance', 
                                  batch_size=args.batch_size, shuffle=True, drop_last=True)
        ind_key = 'dataset_label'
        inputs_split_keys = []
        targets_split_keys = ['heatmap']
        collate_fn = mix_dataset_collate(2, inputs_split_keys, targets_split_keys, ind_key )
        data_loader = torch.utils.data.DataLoader(
                    dataset,
                    batch_sampler=sampler,
                    num_workers=30,
                    pin_memory=True,
                    collate_fn=collate_fn
                )

        val_data_loader_a = torch.utils.data.DataLoader(
        test_dataset_a, 
        batch_size=1, 
        shuffle=False,
        num_workers=4,
        pin_memory=True,
        drop_last=False
        )

        val_data_loader_b = torch.utils.data.DataLoader(
        test_dataset_b, 
        batch_size=1, 
        shuffle=False,
        num_workers=4,
        pin_memory=True,
        drop_last=False
        )
        val_data_loader = [val_data_loader_a, val_data_loader_b]

    use_cuda = torch.cuda.is_available()
    if use_cuda:
        if gpus is None:
            device_name = "cuda"
        else:
            device_name = 'cuda:{}'.format(gpus[0])
    else:
        device_name = 'cpu'
    #device = torch.device( device_name )

    backbone = networks.feature.resnet50()
    in_channel = backbone.out_channel
    neck = networks.neck['upsample_basic'](in_channel)
    backbone.multi_feature = False
    #backbone.out_channel = 256
    #neck = None
    parts = ['heatmap', 'offset', 'width_height']
    #parts = ['heatmap']
    #model = CenterNet(backbone, 1, neck=neck, parts=parts)
    loss_weight = {'heatmap':1., 'offset':1, 'width_height':0.01}
    if dataset_name != 'mix':
        model = CenterNet(backbone, class_num, neck=neck, parts=parts, loss_weight=loss_weight)
    else:
        model = CenterNetMulBranch(backbone, 2, class_nums, parts=parts, neck=neck, loss_weight=loss_weight)

    #load_checkpoint(model, cfg.path.CHECKPOINT.format(150), device)

    #t = my_trainer( cfg, model, device_name, data_loader, testset=val_data_loader, dataset_name='coco_person', benchmark=None, val_dataset=None )
    print('dataset name is {}'.format(dataset_name))
    if dataset_name == 'mix':
        dataset_name = ['coco', 'modanet']
    t = my_trainer( cfg, model, device_name, data_loader, testset=val_data_loader, dataset_name=dataset_name, benchmark=None, val_dataset=None, gpus=gpus )
    #loss = t.trainstep()
    #print('loss is {}'.format(loss))
    #t.train_one_epoch()
    #t.validate_onece()
    #t.load_training(cfg.path.CHECKPOINT.format(150), device_name)
    #t.validate_onece()
    #for inputs, targets in data_loader:
    #    print(inputs.keys())
    #    print(targets.keys())
    #    break
    if dataset_name == 'coco' or dataset_name == 'modanet':
        t.train()
    else:
        t.train_mul_branch()
    #t.train_modanet_human()

    #train_feeder.exit()
    #test_feeder.exit()

    #torch.save({'state_dict':model.state_dict()}, cfg.path.MODEL)

    #print('Model is saved to :', cfg.path.MODEL)
