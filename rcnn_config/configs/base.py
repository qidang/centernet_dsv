from easydict import EasyDict as edict
import numpy as np

def get_cfg() :
    name = 'baseline'

    data = None

    dataset = edict()
    dataset.MIN_OBJ_SIZE = np.array( [ 5,5 ] )
    dataset.SCALE = False
    dataset.SCALES = [ 0.75, 2.0 ]
    dataset.WORKSET_SETTING = ['add_mirrored','randomize_scales','prune_by_size']
    #dataset.WORKSET_SETTING_DEPLOY = ['add_mirrored']
    dataset.WORKSET_SETTINGS_DEPLOY = []

    #dataset.CATEGORIES = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16,
    #                      17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31,
    #                      32, 33, 34, 35, 36, 38, 39, 40, 41, 42, 43, 45, 46]

    train = edict()
    test = edict()

    COMMON = edict()
    COMMON.PIXEL_MEANS = np.array( [[[102.9801, 115.9465, 122.7717]]], dtype=np.float32 )
    COMMON.COLORSPACE = 'rgb'

    train.COMMON = COMMON
    test.COMMON = COMMON

    train.MAX_SIZE = 300
    train.RANDOM_SCALE = True
    train.SCALE_RANGE = [ 0.5, 1.5 ]
    train.MIN_OBJ_SIDE = 10
    train.NITER = 10
    train.OPTIMIZER = {'type':'GD','lr':0.01,'decay_step':5,'decay_rate':0.1}
    train.USE_NEGATIVES = True

    test.MAX_SIZE = 300

    # Proposal Config

    train.rpn = edict()

    # for anchor generator
    train.rpn.anchor_sizes = ((32,), (64,), (128,), (256,), (512,))
    train.rpn.aspect_ratios = ((0.5, 1.0, 2.0),) * len(train.rpn.anchor_sizes)

    # for rpn head
    train.rpn.fg_iou_thresh = 0.7
    train.rpn.bg_iou_thresh = 0.3
    train.rpn.batch_size_per_image = 256
    train.rpn.positive_fraction = 0.5
    train.rpn.train_pre_nms_top_n = 2000
    train.rpn.train_post_nms_top_n = 2000
    train.rpn.test_pre_nms_top_n = 1000
    train.rpn.test_post_nms_top_n = 1000
    train.rpn.nms_thresh = 0.7
    
    test.rpn = train.rpn
    
    # for roi head
    train.roi_head = edict()
    train.roi_head.fg_iou_thresh = 0.5
    train.roi_head.bg_iou_thresh = 0.5
    train.roi_head.batch_size_per_image = 512
    train.roi_head.positive_fraction = 0.25
    train.roi_head.reg_weights = None
    train.roi_head.score_thresh = 0.05
    train.roi_head.nms_thresh = 0.5
    train.roi_head.detections_per_image = 100


    test.roi_head = edict()
    test.roi_head.fg_iou_thresh = 0.5
    test.roi_head.bg_iou_thresh = 0.5
    test.roi_head.batch_size_per_image = 512
    test.roi_head.positive_fraction = 0.25
    test.roi_head.reg_weights = None
    test.roi_head.score_thresh = 0.001
    test.roi_head.nms_thresh = 0.5
    test.roi_head.detections_per_image = 100


    #train.PROPOSAL = edict()
    #train.PROPOSAL.SCORE_THRESH = 0.5
    #train.PROPOSAL.NMS_THRESH = 0.8
    #train.PROPOSAL.POST_NMS_ROIS = 2000
    #train.PROPOSAL.SELECTION_BATCH_SIZE = 64
    #train.PROPOSAL.FG_RATIO = 0.5
    #train.PROPOSAL.WEIGHTS = {'scores':1.0, 'rois':0.01}
    #train.PROPOSAL.AREA_THRESH = np.array([ 32, 96 ]) ** 2
    #train.PROPOSAL.ASPECT_THRESH =  [ 0.8, 2.0 ]
    #train.PROPOSAL.NPARTS = 9
    #train.PROPOSAL.NCLASSES = 1
    #train.PROPOSAL.ADD_GTBOXES = True
    #train.PROPOSAL.REMOVE_SMALL = True


    #test.PROPOSAL = edict()
    #test.PROPOSAL.SCORE_THRESH = 0.5
    #test.PROPOSAL.NMS_THRESH = 0.8
    #test.PROPOSAL.POST_NMS_ROIS = 300
    #test.PROPOSAL.NPARTS = 9
    #test.PROPOSAL.NCLASSES = 1
    #test.PROPOSAL.ADD_GTBOXES = False
    #test.PROPOSAL.REMOVE_SMALL = True

    ## FRCNN Config

    #train.FRCNN = edict()
    #train.FRCNN.NMS = True
    #train.FRCNN.NMS_THRESH = 0.3
    #train.FRCNN.POST_NMS_ROIS = 500
    #train.FRCNN.POOLING = edict()
    #train.FRCNN.POOLING.HEIGHT = 7
    #train.FRCNN.POOLING.WIDTH = 7
    #train.FRCNN.POOLING.FC_SIZE = 4096
    #train.FRCNN.ADD_GTBOXES = True

    #train.FRCNN.FG_MIN_OVERLAP = 0.5
    #train.FRCNN.BG_MAX_OVERLAP = 0.2
    #train.FRCNN.SELECTION_BATCH_SIZE = 32
    #train.FRCNN.FG_RATIO = 0.5
    #train.FRCNN.WEIGHTS = {'scores':1.0, 'deltas':0.01}

    #test.FRCNN = edict()

    #test.FRCNN = edict()
    #test.FRCNN.NMS = True
    #test.FRCNN.NMS_THRESH = 0.3
    #test.FRCNN.POST_NMS_ROIS = 500
    #test.FRCNN.POOLING = edict()
    #test.FRCNN.POOLING.HEIGHT = 7
    #test.FRCNN.POOLING.WIDTH = 7
    #test.FRCNN.POOLING.FC_SIZE = 4096
    #test.FRCNN.ADD_GTBOXES = False

    ## Category Network

    #train.CATEGORY = edict()
    #train.CATEGORY.POOLING = edict()
    #train.CATEGORY.POOLING.HEIGHT = 7
    #train.CATEGORY.POOLING.WIDTH = 7
    #train.CATEGORY.POOLING.CONVS_OUT = 128
    #train.CATEGORY.POOLING.FC_SIZE = 4096
    #train.CATEGORY.POOLING.METHOD = 'bilinear'
    #train.CATEGORY.NCATEGORIES = 3
    #train.CATEGORY.WEIGHTS ={'scores':0.1, 'points':0.01}

    #test.CATEGORY = edict()
    #test.CATEGORY.POOLING = edict()
    #test.CATEGORY.POOLING.HEIGHT = 7
    #test.CATEGORY.POOLING.WIDTH = 7
    #test.CATEGORY.POOLING.FC_SIZE = 4096
    #test.CATEGORY.POOLING.METHOD = 'bilinear'
    #test.CATEGORY.NCATEGORIES = 48


    network = edict()

    network.INTERFACE = None
    network.FEAT_NAME = 'vgg16_small'
    network.PRETRAINED = None
    network.PREFIX = 'detector'
    network.PREFIX_MOD = None
    network.TAG = None
    network.NCLASSES = 1
    network.BATCH_SIZE = 2
    network.CLASSES = {'person':1,'face':2}

    cfg = edict()
    cfg.NAME = name
    cfg.DATA = data
    cfg.DATASET = dataset
    cfg.NETWORK = network
    cfg.DNN = {}
    cfg.DNN['train'] = train
    cfg.DNN['test'] = test

    return cfg
