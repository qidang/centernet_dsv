from . import base as baseline
from . import frcnn_coco as frcnn_coco
from . import frcnn_modanet as frcnn_modanet
from . import yolo
from . import center_net_coco


configurations = {}

configurations['baseline'] = baseline
configurations['frcnn_coco'] = frcnn_coco
configurations['frcnn_modanet'] = frcnn_modanet
configurations['yolo'] = yolo
configurations['center_net_coco'] = center_net_coco
