from easydict import EasyDict as edict
import numpy as np
from . import base as baseline

def get_cfg():
    cfg = baseline.get_cfg()

    # Modifications to baseline config
    cfg.NAME = 'frcnn_modanet'
    cfg.DATASET.WORKSET_SETTING = []

    #cfg.DNN['train'].PATCH_SIZE = 64
    #cfg.DNN['test'].PATCH_SIZE = 64

    # It seems that 10 iteration should be enough
    cfg.DNN['train'].NITER = 12

    #old one
    #cfg.DNN['train'].OPTIMIZER = {'type':'GD','lr':0.01,'decay_step':5,'decay_rate':0.1}
    cfg.DNN['train'].OPTIMIZER = {'type':'GD','lr':0.001,'decay_step':7,'decay_rate':0.1}

    #cfg.NETWORK.FEATURE_DIM = 1024

    ##cfg.DNN.PROPOSAL.TRAIN.SELECTION_BATCH_SIZE = {'s1':128,'s2':128}
    ##cfg.DNN.FRCNN.TRAIN.SELECTION_BATCH_SIZE = {'s1':256,'s2':256,'s2_hinge':2000}

    #cfg.NETWORK.INTERFACE = 'patch'
    #cfg.NETWORK.FEAT_NAME = 'vgg16'

    return cfg