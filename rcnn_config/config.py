from easydict import EasyDict as edict
import sys
import os
import platform
import numpy as np
from pprint import pprint

def mkdir( d ):
    if not os.path.isdir( d ) :
        os.mkdir( d )

class config :
    def __init__( self, cfg_name, mode, datasetpath, projectpath ):
        self._cfg_name = cfg_name
        self._cfg = None
        self._mcfg = None

        self.initialize( mode )
        self.initialize_dataset( datasetpath )
        self.initialize_data( projectpath )

        self._init_detector()

    def initialize( self, mode ):
        from .configs import configurations
        assert self._cfg_name in configurations, "Unknown config"

        cfg = configurations[ self._cfg_name ].get_cfg()
        assert cfg.NAME == self._cfg_name, "Config name does not match"

        self._cfg = edict()

        self._cfg.NAME = cfg.NAME
        self._cfg.DATA = cfg.DATA
        self._cfg.DATASET = cfg.DATASET

        self._cfg.DNN = cfg.DNN[ mode ]
        self._cfg.DNN.NETWORK = cfg.NETWORK

    def initialize_dataset( self, datasetpath ):
        self._cfg.DATASET.ROOT = datasetpath
        self._cfg.DATASET.ANNOTS_TMP = "%s/annotations/%s.pkl" % ( self._cfg.DATASET.ROOT, '%s' )
        self._cfg.DATASET.IMAGES_TMP = "%s/datasets/%s" % ( self._cfg.DATASET.ROOT,'%s' )
        self._cfg.DATASET.WEIGHTS_TMP = "%s/weights/%s" % ( self._cfg.DATASET.ROOT, '%s' )
        self._cfg.DATASET.PRETRAINED = "%s/pretrained/%s.ckpt" % ( self._cfg.DATASET.ROOT, '%s' )
        self._cfg.DATASET.WORKSET_SETTINGS = []

        if self._cfg.DNN.NETWORK.PRETRAINED is not None :
            self._cfg.DATASET.PRETRAINED = self._cfg.DATASET.PRETRAINED % ( self._cfg.DNN.NETWORK.PRETRAINED )

    def initialize_data( self, projectpath ):
        tmp = edict()

        tmp.PROJECT = "embedding"
        tmp.TAG = self._cfg.NAME

        tmp.DIRS = edict()
        tmp.DIRS.ROOT = projectpath
        tmp.DIRS.BASE = os.path.join( tmp.DIRS.ROOT, tmp.PROJECT )

        mkdir( tmp.DIRS.ROOT )
        mkdir( tmp.DIRS.BASE )

        tmp.DIRS.BASE = os.path.join( tmp.DIRS.BASE, tmp.TAG )
        mkdir( tmp.DIRS.BASE )
        tmp.DIRS.MODELS = os.path.join( tmp.DIRS.BASE, "models" )
        mkdir( tmp.DIRS.MODELS )
        tmp.DIRS.DETS = os.path.join( tmp.DIRS.BASE, "dets" )
        mkdir( tmp.DIRS.DETS )
        tmp.DIRS.CHECKPOINTS = os.path.join( tmp.DIRS.BASE, "checkpoints" )
        mkdir( tmp.DIRS.CHECKPOINTS )
        tmp.DIRS.BENCHMARK = os.path.join( tmp.DIRS.BASE, "benchmark" )
        mkdir( tmp.DIRS.BENCHMARK )
        tmp.DIRS.HDF5 = os.path.join( tmp.DIRS.BASE, "hdf5" )
        mkdir( tmp.DIRS.HDF5 )

        self._cfg.DATA = tmp

    def _init_detector( self ):
        model = edict()
        model.CLS_LABELS = {'person':1,'face':2}
        model.NAME = 'end2end'
        model.PATH = '%s/detection/conf_proposal/models/end2end_vgg16_small_CWpetr_20180218.pkl' % ( self._cfg.DATA.DIRS.ROOT )

        dnn = edict()

        dnn.COMMON = edict()
        dnn.COMMON.PIXEL_MEANS = np.array( [[[102.9801, 115.9465, 122.7717]]], dtype=np.float32 )
        dnn.COMMON.COLORSPACE = 'rgb'
        dnn.COMMON.FEAT_MAP_SCALE = -1#1.0/16.0
        dnn.COMMON.BASE_SIZE = -1#16.0

        dnn.DEPLOY = edict()
        dnn.DEPLOY.MAX_SIZE = 300
        dnn.DEPLOY.THRESHOLDS = {'person':0.8,'face':0.9}

        dnn.PROPOSAL = edict()
        dnn.PROPOSAL.SCORE_THRESH = 0.5
        dnn.PROPOSAL.NMS_THRESH = 0.8
        dnn.PROPOSAL.POST_NMS_ROIS = 500
        dnn.PROPOSAL.SCORE_THRESH = 0.5

        dnn.FRCNN = edict()
        dnn.FRCNN.NMS = True
        dnn.FRCNN.NMS_THRESH = 0.3
        dnn.FRCNN.POST_NMS_ROIS = 500
        dnn.FRCNN.FC_SIZE = 4096
        dnn.FRCNN.POOL_HEIGHT = 6
        dnn.FRCNN.POOL_WIDTH = 6

        dnn.NETWORK = edict()
        dnn.NETWORK.FEAT_NAME = 'vgg16_small'
        dnn.NETWORK.FEAT_INIT = None
        dnn.NETWORK.PREFIX = 'personface'
        dnn.NETWORK.PREFIX_MOD = None
        dnn.NETWORK.TAG = None
        dnn.NETWORK.NCLASSES = 2
        dnn.NETWORK.BATCH_SIZE = 1

        self._cfg.DETECTOR = edict()
        self._cfg.DETECTOR.model = model
        self._cfg.DETECTOR.dnn = dnn


    def build_path( self, tag, data_hash, model_hash=None ):
        assert( self._cfg.DATA is not None )
        data = self._cfg.DATA
        dnn = self._cfg.DNN

        if model_hash is None :
            model_hash = data_hash

        path = edict()
        path.MODEL = '%s/model_%s_%s_%s.pkl' % ( data.DIRS.MODELS, dnn.NETWORK.FEAT_NAME, model_hash, tag )
        path.HDF5 = '%s/%s.hdf5' % ( data.DIRS.HDF5, '%s' )
        path.LOG = '%s/log_%s_%s_%s.csv' % ( data.DIRS.MODELS, dnn.NETWORK.FEAT_NAME, model_hash, tag )
        path.RES  = '%s/results_model_%s_dset_%s_%s.pkl' % ( data.DIRS.DETS, model_hash, data_hash, tag )
        path.BENCHMARK = '%s/bench_model_%s_dset_%s_%s_%s.pkl' % ( data.DIRS.BENCHMARK, model_hash, data_hash, tag, '%s' )
        path.BENCHMARK_LOG = '%s/bench_log_%s_%s_%s.csv' % ( data.DIRS.BENCHMARK, dnn.NETWORK.FEAT_NAME, model_hash, tag )
        path.CHECKPOINT = '{}/checkpoints_{}_{{}}.pkl'.format(data.DIRS.CHECKPOINTS, tag)

        self._cfg.PATH = path

    def update( self, params ):
        nclasses = params.get('nclasses',None)
        if nclasses is not None :
            self._cfg.DNN.NETWORK.NCLASSES = int(nclasses)

        batch_size = params.get('batch_size',None)
        if batch_size is not None :
            self._cfg.DNN.NETWORK.BATCH_SIZE = int(batch_size)

        niter = params.get('niter',None)
        if niter is not None :
            self._cfg.DNN.TRAIN.NITER = int(niter)

        prefix = params.get('prefix',None)
        if prefix is not None :
            self._cfg.DNN.NETWORK.PREFIX = prefix

        usenegatives = params.get('usenegatives',None)
        if usenegatives is not None :
            self._cfg.DNN.USE_NEGATIVES = usenegatives

    @property
    def dnn( self ):
        return self._cfg.DNN

    @property
    def data( self ):
        return self._cfg.DATA

    @property
    def dataset( self ):
        return self._cfg.DATASET

    @property
    def path( self ):
        return self._cfg.PATH

    @property
    def detector( self ):
        return self._cfg.DETECTOR
