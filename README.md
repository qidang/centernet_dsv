# Instructions

It is better you set up the ssh key for bitbuket website first so you might have no other problem in the procedure

Please clone this git repo to your computer first

	cd centernet_dsv
	mkdir weights
	git submodule init
	git submodule update --remote

Then download the [model](https://drive.google.com/file/d/1m4zo4IGHL-U8zPWDc33uEHAE9k1QGIKr/view?usp=sharing) to weights

Then try to run the code

	python demo.py

I haven't tested it myself but I hopt it would work.
